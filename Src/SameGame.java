import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

/*
 * Main class of the homework the output the SameGame
 * @author <em>Khoi Le </em>
 */
public class SameGame extends Application {
    private String mode;   // Mode of the game
    private int height;   // Height of the board
    private int width;   // Width of the board
    private int level;   // Number of color in the board
    private Button[][] buttonLayout;   //Button layout
    private int locationX;   // Store X cord of the button
    private int locationY;   // Store Y cord of the button
    private int[] pathX;   //  X cord to run the search
    private int[] pathY;   //  Y cord to run the search
    private int blockLeft;   // Number of block left
    private int columLeft;   //   Number of colum left
    private boolean[][] visit;   // Array of boolean used for the algorithm
    private GridPane pane = new GridPane();   // The main pane
    private TextField text = new TextField();   // Text field for the output
    private Stage result;   // new window for the output
    private Stage primaryStage; // Main window of the program
    /*
     * Start method of the javafx program
     * @param primaryStage the initial stage of the program
     */
    @Override
    public void start(Stage primaryStage){
        generateMap();
        primaryStage.setTitle("Same Game");
        pane = new GridPane();
        // Loop to add the button into button layout
        for(int i = 0; i < this.height; i++){
            for(int j = 0; j < this.width; j++){
                pane.add(this.buttonLayout[i][j],j,i);
            }
        }
        Scene scene = new Scene(pane);
        primaryStage.setScene(scene);
        primaryStage.show();
        result = new Stage();
        BorderPane resultPane = new BorderPane();
        resultPane.setCenter(text);
        text.setEditable(false);
        Scene resultScene = new Scene(resultPane);
        result.setScene(resultScene);
    }

    /*
     * Nested class that handle the button click in the game
     */
    public class ClickHandle implements EventHandler<ActionEvent>{

        /*
         * Override handle method of the EventHandler
         * @param e ActionEvent of the button click
         */
        public void handle(ActionEvent e){
            Button b = (Button)e.getSource();   // Button clicked
            locationX = GridPane.getColumnIndex(b);
            locationY = GridPane.getRowIndex(b);
            for(boolean[] a: visit){
                Arrays.fill(a, false);
            }
            if(!((Color)((Circle)buttonLayout[locationY][locationX].getGraphic()).getFill()).equals(Color.LIGHTGRAY)) {
                if ((mode.equals("plus") && haveCross(locationY, locationX)) || (mode.equals("star") && haveStar(locationY, locationX)) || (mode.equals("same") && haveSame(locationY, locationX))) {
                    visit[locationY][locationX] = true;
                    for (int i = 0; i < height; i++) {
                        for (int j = 0; j < width; j++) {
                            if (visit[i][j]) {
                                delete(i, j);
                            }
                        }                      
                    }
                    checkMissingColum();
                    if (blockLeft == 0) {
                        text.setText(" You WIN");
                        result.show();
                    } else if (noMoveLeft()) {
                        text.setText(" You LOSE");
                        result.show();
                    }
                }
            }

        }
    }
    
    /*
     * Check if there is no move left
     * @return boolean is there any move left
     */ 
    public boolean noMoveLeft(){
        // Loop through the board to check if there is move left
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
                if(!((Color)((Circle)buttonLayout[i][j].getGraphic()).getFill()).equals(Color.LIGHTGRAY) && ((mode.equals("plus") && haveCross(i, j)) || (mode.equals("star") && haveStar(i, j)) || (mode.equals("same") && haveSame(i, j)))){
                    return false;
                }
            }
        }
        return true;
    }

    /*
     * Method to check if there is a missing colum to remove it
     */
    public void checkMissingColum(){
        // Loop through the colums to find if any colum is missing to remove it
        for(int i = 0; i < columLeft; i++){
            int j = 0;
            // Loop through each piece in the colum to see if the colum is all gray
            while(j < height && (((Circle)buttonLayout[j][i].getGraphic()).getFill()).equals(Color.LIGHTGRAY)){
                j++;
            }
            if(j == height){
                copyColum(i);
                i--;
            }
        }
    }

    /*
     * Method to move colum to the right of the colum 1 colum left
     * @param colum Colum that is deleted
     */
    public void copyColum(int colum){
        if (colum < columLeft - 1) {
            // Loop through the right right colum and copy it to the left
            for (int i = 0; i < height; i++) {
                ((Circle) buttonLayout[i][colum].getGraphic()).setFill(((Circle) buttonLayout[i][colum + 1].getGraphic()).getFill());
            }
            copyColum(colum + 1);
        }
        else{
            // Loop through the right colum to set it to gray when there is no right colum
            for (int i = 0; i < height; i++) {
                ((Circle)buttonLayout[i][colum].getGraphic()).setFill(Color.LIGHTGRAY);
            }
            columLeft--;
        }
    }

    /*
     * Method to delete a button
     * @param yCord, xCord : the coordinate of the button that need to delete
     */
    public void delete( int yCord, int xCord){
        // Loop run from the point up to drop button rom above down 1 button
        for(int i = yCord; i >= 0; i--){
            if(i-1 >= 0){
                ((Circle)buttonLayout[i][xCord].getGraphic()).setFill((Color)((Circle)buttonLayout[i-1][xCord].getGraphic()).getFill());
            }
            else{
                ((Circle)buttonLayout[i][xCord].getGraphic()).setFill(Color.LIGHTGRAY);
            }
        }
        blockLeft--;

    }

    /*
     * Main method of the class
     * @param args: array of String
     */
    public static void main(String[] args) {
        launch(args);
    }

    /*
     * Method to check if it meet the requirement of the plus game mode
     * @param color : color to check
     * @return boolean if it meet the requirement or not
     */
    public boolean haveCross(int y, int x){
        Color color = (Color)((Circle)buttonLayout[y][x].getGraphic()).getFill();   // Color of the button
        int result = 0;  //  variable to check how many adj buttons have the same color
        //  Loop from 1 to 4 of the path array to find all the cordinate that can be reached
        for(int i = 0; i < 4; i++){
            int j = 1;
            while(inMap(y + pathY[i]*j,x + pathX[i]*j) && ((Circle)buttonLayout[y + pathY[i]*j][x + pathX[i]*j].getGraphic()).getFill().equals(color)){
                result++;
                visit[y + pathY[i]*j][x + pathX[i]*j] = true;
                j++;
            }
        }
        return (result > 0);
    }

    /*
     * Method to check if it meet the requirement of the star game mode
     * @param color : color to check
     * @return boolean if it meet the requirement or not
     */
    public boolean haveStar(int y, int x){
        Color color = (Color)((Circle)buttonLayout[y][x].getGraphic()).getFill();   // Color of the button
        int result = 0;  // variable ot check how many adj botton have the same color
        // Loop from 1 to 8 of the path array to find all the cordinate that can be reached
        for(int i = 0; i < 8; i++){
            int j = 1;
            while(inMap(y + pathY[i]*j,x + pathX[i]*j) && ((Circle)buttonLayout[y + pathY[i]*j][x + pathX[i]*j].getGraphic()).getFill().equals(color)){
                visit[y + pathY[i]*j][x + pathX[i]*j] = true;
                result++;
                j++;
            }
        }
        return (result > 0);
    }

    /*
     * Method to check if it meet the requirement of the same game mode using breadth first search
     * @param color the color to check
     * @return boolean if it meet the requirement
     */
    public boolean haveSame(int y, int x){
        Color color = (Color)((Circle)buttonLayout[y][x].getGraphic()).getFill();   // Color of the button
        int result = 1;  // variable to check how many adj button that have the same color
        LinkedList<Integer> blockY = new LinkedList<Integer>();   // Linked list to store the y-cord to check
        LinkedList<Integer> blockX = new LinkedList<Integer>();   // Linked list to store the x-cord to check
        // Loop to fill the visit array false for the algorithm
        blockY.add(y);
        blockX.add(x);
        // Loop until there is no block can be reach more to tag them
        while(! blockY.isEmpty()){
            int tempY = blockY.removeFirst();
            int tempX = blockX.removeFirst();
            // Loop from 1 to 4 of the path array to find the next path
            for(int i = 0; i < 4; i ++){
                if(inMap(tempY + pathY[i],tempX + pathX[i]) && ! visit[tempY + pathY[i]][tempX + pathX[i]] && (((Circle)buttonLayout[tempY + pathY[i]][tempX + pathX[i]].getGraphic()).getFill().equals(color))){
                    result++;
                    visit[tempY + pathY[i]][tempX + pathX[i]] = true;
                    blockY.addLast(tempY + pathY[i]);
                    blockX.addLast(tempX + pathX[i]);
                }
            }
        }
        return (result > 1);
    }

    /*
     * Method to check if the x,y cord is in the board or not
     * @param x,y the coordinate
     * @return boolean if it is in map or not
     */
    public boolean inMap(int i, int j) {
        return (i >= 0 && j >= 0 && i < height && j < width);
    }

    /*
     * Method to initialize every component
     */
    public void generateMap(){
        try{
            mode = this.getParameters().getRaw().get(0);
            height = Integer.parseInt(this.getParameters().getRaw().get(1));
            width = Integer.parseInt(this.getParameters().getRaw().get(2));
            level = Integer.parseInt(this.getParameters().getRaw().get(3));
        }
        catch(RuntimeException error){
            System.out.println("you dont have enough input");
        }
        if(level > 9 || level < 1) {
            System.out.println("Input level fron 1 to 9");
        }
        if(!mode.equals("plus") && !mode.equals("star") && !mode.equals("same")){
            System.out.println("Wrong game mode");
        }
        blockLeft = height * width;
        this.buttonLayout = new Button[this.height][this.width];
        visit = new boolean[height][width];
        columLeft = width;
        pathX = new int[]{0, 1, 0, -1, 1, 1, -1, -1};
        pathY = new int[]{-1, 0, 1, 0, -1, 1, 1, -1};
        Color[] colorArray = new Color[]{Color.RED, Color.BLUE, Color.GREEN, Color.ORANGE, Color.YELLOW, Color.PURPLE, Color.LIGHTBLUE, Color.LIGHTGREEN, Color.PINK};
        // Loop through the map to generate random color
        for(int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                buttonLayout[i][j] = new Button();
                Circle circle = new Circle(15);
                circle.setFill(colorArray[(int) (Math.random() * level)]);
                buttonLayout[i][j].setGraphic(circle);
                buttonLayout[i][j].setOnAction(new ClickHandle());
            }
        }
    }}
